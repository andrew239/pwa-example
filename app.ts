import express, { Request,Response } from 'express';

const app = express();

app.get("/all_todo",(req:Request,res:Response)=>{
    return res.json([
        { id:1,text:"TODO#1"},
        { id:2,text:"TODO#2"},
        { id:3,text:"TODO#3"},
        { id:4,text:"TODO#4"},
        { id:5,text:"TODO#5"}
    ]);
})
app.use(express.static("public"));
app.listen(8080,()=>{
    console.log("TODO List Application is ready.");
});