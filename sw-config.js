//Wrire down your own service worker requirements
//------------------------------------------------//
/*
To let offline mode workable, workbox must be stored in local, and then it is 
imported locally
*/
importScripts('third_party/workbox/workbox-v6.3.0/workbox-sw.js');
workbox.setConfig( {modulePathPrefix: '/third_party/workbox/workbox-v6.3.0/'} );
//------------------------------------------------//

//Demo Purpose : Workbox usable Parameters
workbox.setConfig({debug:true}); //Development Environment ONLY
workbox.routing.registerRoute(
  /.*\.(?:png|jpg|jpeg|svg|gif)/g,
  new workbox.strategies.CacheFirst({
  cacheName: 'image-cache',
  })
);

workbox.routing.registerRoute(
  /.*\.(?:html)/g,
  new workbox.strategies.CacheFirst({
      cacheName: 'html-cache'
  })
);

workbox.routing.registerRoute(
  /.*\.(?:css)/g,
  new workbox.strategies.CacheFirst({
      cacheName: 'css-cache'
  })
);

workbox.routing.registerRoute(
/.*\.(?:json|js)/g,
new workbox.strategies.CacheFirst({
    cacheName: 'js-cache'
})
);

workbox.routing.registerRoute(
  /\/all_todo/,
  new workbox.strategies.CacheFirst({
    cacheName: 'fetch-todo-ws',
    plugins: [
      new workbox.cacheableResponse.CacheableResponsePlugin({
        statuses: [0, 200],
      }),
      new workbox.expiration.ExpirationPlugin({
        maxAgeSeconds: 60 * 60,
        maxEntries: 2
      }),
    ],
  })
);

workbox.precaching.precacheAndRoute(self.__WB_MANIFEST);