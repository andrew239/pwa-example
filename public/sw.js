//Wrire down your own service worker requirements
//------------------------------------------------//
/*
To let offline mode workable, workbox must be stored in local, and then it is 
imported locally
*/
importScripts('third_party/workbox/workbox-v6.3.0/workbox-sw.js');
workbox.setConfig( {modulePathPrefix: '/third_party/workbox/workbox-v6.3.0/'} );
//------------------------------------------------//

//Demo Purpose : Workbox usable Parameters
workbox.setConfig({debug:true}); //Development Environment ONLY
workbox.routing.registerRoute(
  /.*\.(?:png|jpg|jpeg|svg|gif)/g,
  new workbox.strategies.CacheFirst({
  cacheName: 'image-cache',
  })
);

workbox.routing.registerRoute(
  /.*\.(?:html)/g,
  new workbox.strategies.CacheFirst({
      cacheName: 'html-cache'
  })
);

workbox.routing.registerRoute(
  /.*\.(?:css)/g,
  new workbox.strategies.CacheFirst({
      cacheName: 'css-cache'
  })
);

workbox.routing.registerRoute(
/.*\.(?:json|js)/g,
new workbox.strategies.CacheFirst({
    cacheName: 'js-cache'
})
);

workbox.routing.registerRoute(
  /\/all_todo/,
  new workbox.strategies.CacheFirst({
    cacheName: 'fetch-todo-ws',
    plugins: [
      new workbox.cacheableResponse.CacheableResponsePlugin({
        statuses: [0, 200],
      }),
      new workbox.expiration.ExpirationPlugin({
        maxAgeSeconds: 60 * 60,
        maxEntries: 2
      }),
    ],
  })
);

workbox.precaching.precacheAndRoute([{"revision":"42c986d67ee12d274c2615771745193e","url":"manifest.json"},{"revision":"f3ed69940b54dd59a27192e53911c126","url":"third_party/workbox/workbox-v6.3.0/workbox-background-sync.dev.js"},{"revision":"94c01774e3ed2d91deb05d103ff9733d","url":"third_party/workbox/workbox-v6.3.0/workbox-background-sync.prod.js"},{"revision":"0ad0a104a66be635ce29bb371428db29","url":"third_party/workbox/workbox-v6.3.0/workbox-broadcast-update.dev.js"},{"revision":"0ec9d9c6d662e16dc83fe47ef9ed877c","url":"third_party/workbox/workbox-v6.3.0/workbox-broadcast-update.prod.js"},{"revision":"8aad5781bee32a1ce1ab9436d4b57476","url":"third_party/workbox/workbox-v6.3.0/workbox-cacheable-response.dev.js"},{"revision":"6245e5001a6cba8bcbd5129e7b1014de","url":"third_party/workbox/workbox-v6.3.0/workbox-cacheable-response.prod.js"},{"revision":"0c883c713ae039f4e57d411edca648f1","url":"third_party/workbox/workbox-v6.3.0/workbox-core.dev.js"},{"revision":"465d9a2135df94b80636ec6b6d9c65ee","url":"third_party/workbox/workbox-v6.3.0/workbox-core.prod.js"},{"revision":"423434f297933662cc0eb7599549b24a","url":"third_party/workbox/workbox-v6.3.0/workbox-expiration.dev.js"},{"revision":"8d7a7ebe243aab8a8454ae53d451483f","url":"third_party/workbox/workbox-v6.3.0/workbox-expiration.prod.js"},{"revision":"ec983ba6d4d16babeea4d8ee62fba069","url":"third_party/workbox/workbox-v6.3.0/workbox-navigation-preload.dev.js"},{"revision":"afc8bde96d002ed321d73e998f1dff9c","url":"third_party/workbox/workbox-v6.3.0/workbox-navigation-preload.prod.js"},{"revision":"af97224f5b736e318492e2ee755855e6","url":"third_party/workbox/workbox-v6.3.0/workbox-offline-ga.dev.js"},{"revision":"13ffbe9620a7aaa2e5386325e5e349f1","url":"third_party/workbox/workbox-v6.3.0/workbox-offline-ga.prod.js"},{"revision":"05449941c13ad31b6379bd85d0988bb2","url":"third_party/workbox/workbox-v6.3.0/workbox-precaching.dev.js"},{"revision":"17f9400f50e92f330c81988d2fc1574a","url":"third_party/workbox/workbox-v6.3.0/workbox-precaching.prod.js"},{"revision":"337fc06c4bb07cbfde8e9c0e01b7ec59","url":"third_party/workbox/workbox-v6.3.0/workbox-range-requests.dev.js"},{"revision":"54069132660dc699dffc1c5835188401","url":"third_party/workbox/workbox-v6.3.0/workbox-range-requests.prod.js"},{"revision":"0f1b137a1741d96c011cbd73026f7539","url":"third_party/workbox/workbox-v6.3.0/workbox-recipes.dev.js"},{"revision":"7156650296842821524d54c4eb95d5ac","url":"third_party/workbox/workbox-v6.3.0/workbox-recipes.prod.js"},{"revision":"f3373f1015e576b97ca979609c03f81f","url":"third_party/workbox/workbox-v6.3.0/workbox-routing.dev.js"},{"revision":"c7244262d4207766dcd58a9fd28112e5","url":"third_party/workbox/workbox-v6.3.0/workbox-routing.prod.js"},{"revision":"7dc34cd7ab84f0d296f2f3dd3785afea","url":"third_party/workbox/workbox-v6.3.0/workbox-strategies.dev.js"},{"revision":"68253e40a1797d84f870d3754f13d228","url":"third_party/workbox/workbox-v6.3.0/workbox-strategies.prod.js"},{"revision":"9bc3bd04fd395fd9aad1c60f7fb40f42","url":"third_party/workbox/workbox-v6.3.0/workbox-streams.dev.js"},{"revision":"916ddd22a44fb1345a1cddc50169548a","url":"third_party/workbox/workbox-v6.3.0/workbox-streams.prod.js"},{"revision":"7e3a5b94e8ca94d21e65dffd56e3a416","url":"third_party/workbox/workbox-v6.3.0/workbox-sw.js"},{"revision":"12fa36c6a779c84f5728e95976df2033","url":"third_party/workbox/workbox-v6.3.0/workbox-window.dev.umd.js"},{"revision":"572f7b5fd368befd58e126a6d3a71d64","url":"third_party/workbox/workbox-v6.3.0/workbox-window.prod.umd.js"},{"revision":"77a5f92445feafd9579498c5a0bbb662","url":"todo.css"},{"revision":"855d4bda6414ec3b503f37bda1bfb835","url":"todo.html"},{"revision":"9c9cabc74d99d8f8f3bc8674f3ab5498","url":"todo.js"}]);