if ('serviceWorker' in navigator) {
    window.addEventListener('load', async()=>{
        const res = await navigator.serviceWorker.register('/sw.js');
        //console.log(res.scope);
        // navigator.serviceWorker.getRegistrations().then(function (reg) {
        //     for (var reg of reg) {
        //         reg.unregister()
        //     }
        // });
    });
}

(async()=>{
    const res = await fetch("/all_todo");
    const todo = await res.json();
    const todoPanel = document.querySelector("div#todos");
    for(let todoItem of todo){
        todoPanel.innerHTML += `<div class="col-md-3">
          <div class="todoBox">${todoItem.text}</div>
        </div>`
    }
})();