module.exports = {
	globDirectory: 'public/',
	globPatterns: [
		'**/*.{json,js,css,html}'
	],
	swDest: 'public/sw.js',
	swSrc: 'sw-config.js'
};